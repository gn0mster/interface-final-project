<html>
<head>
<script> 
window.setTimeout(
		"document.location='index.jsp'", 2500);

</script>
<link href='http://fonts.googleapis.com/css?family=Architects+Daughter'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="stylesheet01.css" type="text/css"/>

<style>
body {
background-image: url('images/background2.jpg');
background-repeat: no-repeat;
background-position: center top;
font-family: 'Architects Daughter', cursive;
color: #15b0b4;
text-align: center; 

}

#logout {
	display: block; 
	font-family: 'Architects Daughter', cursive;
	font-size: 1.10em; 
	color: #15b0b4;
	background-color: rgba(0,0,0,0.5);
	margin-top: 15px; 
	width: 30ch; 
	margin-left: auto;
	margin-right: auto; 
		border-radius: 25px;
		padding-top: 0.5em; 
		padding-bottom: .5em; 
}


</style>

</head>
<body> 
<div id= "signonheader"> <img id="logo" src= "images/interfacebooklogo.png"/></div>
<div id="logout"> 
<p>User '<%=request.getRemoteUser()%>' has been logged out.</p>

<% session.invalidate(); %>

<a href="/book">Back to Interfacebook App</a>
</div>
</body>
</html>