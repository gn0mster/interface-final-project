package com.twinfeats.interfacebook.web;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Servlet implementation class MediaUpload handles uploading of files, storing them, and fetching them for the browser.
 * If the incoming request is a multipart encoded form, then the data is incoming file data.
 * If not then the PATHINFO of the request specifies the already uploaded file to pull.
 */
@WebServlet(name="MediaUpload", urlPatterns ="/uploads/*", initParams = @WebInitParam(name="root", value="/interface/book/uploads"))
public class MediaUpload extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * root is the physical file path on disk where uploaded files will be placed. This folder does not need to exist
     * in advance, it will be created on demand.
     */
	private String root = "";
    private static Logger logger = LoggerFactory.getLogger(MediaUpload.class);
    
    /**
     * lastUpload tracks the time of the last uploaded file, from anyone. This resource is protected with
     * synchronized blocks. The use of lastUpload is to guarantee the uniqueness of filenames being uploaded, since
     * the current timestamp will be applied to the file. If two file uploads complete within the same millisec,
     * one will wait for 1 millisec to enforce this uniqueness.
     */
    private long lastUpload = 0;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MediaUpload() {
    }

	/**
	 * Called by Tomat on the first access to this servlet to initialize any data, typically from init parameters
	 * specified either in web.xml or as annotations on the servlet.
	 */
    @Override
	public void init(ServletConfig config) throws ServletException {
		root = config.getInitParameter("root");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (ServletFileUpload.isMultipartContent(request)) {
			//upload request
			handleUpload(request,response,"/images");
			return;
		}
		else {
			//retrieval request
			handleSend(request, response);
		}
	}

	/**
	 * Process the file upload(s), save them to disk, and generate a JSON response indicating the URI to the resource
	 * that can be used in the web page to access the uploaded file.
	 * @param request
	 * @param response
	 * @param dest
	 */
	protected void handleUpload(HttpServletRequest request, HttpServletResponse response, String dest) {
		File f = new File(root+dest);
		f.mkdirs();
		ServletFileUpload upload = new ServletFileUpload();

		JSONObject json = new JSONObject();
		PrintWriter writer = null;
		try {
			writer = response.getWriter();
			FileItemIterator iter = upload.getItemIterator(request);
			while (iter.hasNext()) {
				FileItemStream item = iter.next();
				String name = item.getName();
				if (!item.isFormField()) {
					long now;
					synchronized(MediaUpload.class) {
						now = System.currentTimeMillis();
						//if anther request thread has already been processed in this same millisec timestamp,
						//wait for 1 millisec so that the timestamp put into the name of the uploaded file will
						//be unique.
						if (now == lastUpload) {
							Thread.sleep(1);
						}
						//update timestamp of last processed upload
						lastUpload = now;
					}
					//prefix the filename with the timestamp for uniqueness
					name = now+name;
					InputStream fstream = null;
					try {
						//open the file stream to the uploaded data and then save it to disk
						fstream = item.openStream();
						saveFile(f,name,fstream);
						//add the URI to access this file to the JSON response
						json.put("filelink",request.getContextPath()+"/uploads"+dest+"/"+name);
					}
					finally {
						if (fstream != null) {
							fstream.close();
						}
					}
				}
			}
		}
		catch (Throwable t) {
			logger.error("Could not process upload",t);
			json.put("error", "Could not process upload, please try again later. Sorry for the inconvenience!");
		}
		writer.print(json.toString());
	}

	/**
	 * Save the uploaded file using it's stream to disk
	 * @param path
	 * @param name
	 * @param stream
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private void saveFile(File path, String name, InputStream stream) throws FileNotFoundException, IOException {
		int idx = name.lastIndexOf("/");
		if (idx != -1) {
			name = name.substring(idx+1);
		}
		File f = new File(path,name);
		if (f.exists()) {
			//throw error?
		}
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(f);
			pipeStream(stream,fos);
		}
		finally {
			if (fos != null) {
				fos.close();
			}
		}
	}

	/**
	 * Retrieve the requested uploaded file and stream it back to the browser.
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	protected void handleSend(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String path = request.getPathInfo();
		logger.debug("handleSend {}",path);
		if (path != null) {
			path = root+path;
			ServletOutputStream output = response.getOutputStream();
			String contentType = getContentType(path);
			if (contentType != null) {
				response.setContentType(contentType);
				File file = new File(path);
				if (file.exists()) {
					FileInputStream input = null;
					BufferedInputStream data = null;
					try {
						input = new FileInputStream(file);
						data = new BufferedInputStream(input);
						pipeStream(input, output);
					}
					finally {
						if (data != null) {
							try {
								data.close();
							}
							catch (Throwable t) {
							}
						}
						if (input != null) {
							try {
								input.close();
							}
							catch (Throwable t) {								
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * Utility method the take all the data from an input stream and write it to the output stream. This method could be
	 * a static method in a utility class since it accesses nothing from this class.
	 * @param input
	 * @param output
	 * @throws IOException
	 */
	private void pipeStream(InputStream input, OutputStream output) throws IOException {
		byte[] buffer = new byte[8192];
		while (true) {
			int len = input.read(buffer);
			if (len == -1) {
				break;
			}
			output.write(buffer, 0, len);
		}
	}
	
	/**
	 * Set the response stream's content type based on the extension of the uploaded file.
	 * @param name
	 * @return
	 */
	private String getContentType(String name) {
		String ext = "";
		int extIndex = name.lastIndexOf('.');
		if (extIndex > -1) {
			ext = name.substring(extIndex+1).toLowerCase();
		}
		if (ext.equals("jpg") || ext.equals("gif") || ext.equals("png")) {
			return("image/"+ext);
		}
		else if (ext.equals("pdf")) {
			return("application/pdf");
		}
		else if (ext.equals("doc")) {
			return("application/msword");
		}
		else if (ext.equals("xls")) {
			return("application/vnd.ms-excel");
		}
		else if (ext.equals("txt")) {
			return("text/plain; charset=UTF-8");
		}
		else if (ext.equals("html") || ext.equals("htm")) {
			return("text/html");
		}
		else if (ext.equals("mov")) {
			return("video/quicktime");
		}
		else if (ext.equals("mp4")) {
			return("video/mp4");
		}
		return null;
	}
}
