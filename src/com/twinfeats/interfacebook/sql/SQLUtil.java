package com.twinfeats.interfacebook.sql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.twinfeats.interfacebook.web.ResultSetHandler;


//test
public class SQLUtil {
	private static DataSource ds = null;
	
	public static void init(String dsname) throws NamingException {
		InitialContext ctx = new InitialContext();
		ds = (DataSource)ctx.lookup("java:comp/env/jdbc/"+dsname);
	}
	
	public static Connection getConnection() throws SQLException {
		return ds.getConnection();
	}
	
	/**
	 * This method is used in place of a doing a SELECT in each class that needs it. It delegates the process of each
	 * row in the result set to the specified ResultSetHandler, but keeps all the other mundane code here in one
	 * place for reuse purposes.
	 * @param sql
	 * @param handler
	 * @return
	 * @throws SQLException
	 */
	public static <T> List<T> executeStatement(String sql, ResultSetHandler<T> handler) throws SQLException {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		List<T> list = new LinkedList<T>();
		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				T o = handler.processRow(rs);
				list.add(o);
			}
			return list;
		}
		finally {
			if (rs != null) {
				try {
					rs.close();
				}
				catch (Throwable t) {					
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				}
				catch (Throwable t) {					
				}
			}
			if (conn != null) {
				try {
					conn.close();
				}
				catch (Throwable t) {					
				}
			}
		}
	}
}